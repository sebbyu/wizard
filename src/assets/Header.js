export const headers = [
  "Before customizing your video, name the project and select the output details.",
  "Choose your color.",
  "Please enter your desired text.",
  "Please confirm your selections before submitting.",
]