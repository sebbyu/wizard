import { createApp } from 'vue'
import App from './App.vue'
// Store
import store from './store';

// createApp(App).use(store).mount('#app')
const app = createApp(App);
app.config.unwrapInjectedRef = true
app.use(store).mount('#app');
