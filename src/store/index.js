import { createStore } from "vuex";

export default createStore({
  state() {
    return {
      userData: {
        projectName: null,
        templateId: 0,
        displayWidth: null,
        displayHeight: null,
        textFields: [],
        colors: "",
        output: null
      },
      currentPage: 1,
      mediaSize: {},
      textFields: {}
    }
  },
  mutations: {
    setProjectName(state, payload) {
      state.userData.projectName = payload;
    },
    setDisplayWidth(state, payload) {
      state.userData.displayWidth = payload;
    },
    setDisplayHeight(state, payload) {
      state.userData.displayHeight = payload;
    },
    addText(state, payload) {
      state.textFields[Object.keys(payload)[0]] = Object.values(payload)[0];
    },
    exportText(state) {
      Object.keys(state.textFields).forEach((x) => {
        state.userData.textFields.push(state.textFields[x]);
      })
    },
    setColor(state, payload) {
      state.userData.colors = payload;
    },
    setOutput(state, payload) {
      state.userData.output = payload;
    },
    addMediaSize(state, payload) {
      state.mediaSize[payload] = 0;
    },
    removeMediaSize(state, payload) {
      delete state.mediaSize[payload];
    },
  },
})

